package com.vuelos.vuelos.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "Vuelo")
public class Vuelo implements Serializable {

    private static final long serialVersionUID = 4677374595400102360L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idItinerary;
    
    @NotNull(message="La Fecha de Salida es obligatoria")
    @Column(name = "FechaSalida")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date fechaSalida;
    
    @NotNull(message="La Fecha de Llegada es obligatoria")
    @Column(name = "FechaLlegada")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date fechaLlegada;
    
    @NotEmpty(message="La Ciudad Origen es obligatoria")
    @Column(name = "CiudadOrigen")
    private String ciudadOrigen;
    
    @NotEmpty(message="La Ciudad Destino es obligatoria")
    @Column(name = "CiudadDestino")
    private String ciudadDestino;
    
    @NotEmpty(message="El Nombre es obligatorio")
    @Column(name = "Nombre")
    private String nombre;
    
    @NotNull(message="La Edad es obligatoria")
    @Column(name = "Edad")
    private int edad;
    
    @NotNull(message="Bodega de Equipaje es obligatorio")
    @Column(name = "BodegaEquipaje")
    private boolean bodegaEquipaje;
    
    @NotNull(message="El Precio es obligatorio")
    @Column(name = "Precio")
    private double precio;
    
    @NotEmpty(message="La Hora Salida es obligatoria")
    @Column(name = "HoraSalida")
    private String horaSalida;
    
    @NotEmpty(message="La Hora Llegada es obligatoria")
    @Column(name = "HoraLlegada")
    private String horaLlegada;
   
}
