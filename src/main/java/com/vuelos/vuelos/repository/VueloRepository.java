package com.vuelos.vuelos.repository;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import com.vuelos.vuelos.entity.Vuelo;

public interface VueloRepository extends JpaRepositoryImplementation<Vuelo,Long>{

}
