package com.vuelos.vuelos.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vuelos.vuelos.entity.Vuelo;
import com.vuelos.vuelos.service.VuelosService;
import com.vuelos.vuelos.util.ResponseGeneric;
import com.vuelos.vuelos.util.ValidacionDatos;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/vuelos")
public class ControllerVuelos {
	
	@Autowired
	VuelosService vueloService;
	
	@GetMapping()
	public ResponseEntity<?> getVuelos() {
		List<Vuelo> listVuelos = vueloService.getVuelos();
		ResponseGeneric<Object> res = new ResponseGeneric<>();
		if(listVuelos != null && !listVuelos.isEmpty()) {
			res.setEstatus(200);
			res.setMensaje("Datos Encontrados.");
			res.setRspDatos(listVuelos);
			return new ResponseEntity<>(res,HttpStatus.OK);
		}else {
			res.setEstatus(404);
			res.setMensaje("No se encuentran registros.");
			return new ResponseEntity<>(res,HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/{id}")
    public ResponseEntity<?> getVuelo(@PathVariable(name = "id", required = true)  Long id) {
		Optional<Vuelo> vuelo = vueloService.getVueloById(id);
        if (vuelo.isPresent()) {
        	ResponseGeneric<Object> res = new ResponseGeneric<>();
        	res.setEstatus(200);
        	res.setMensaje("Datos Encontrados");
        	res.setRspDatos(vuelo);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
        	ResponseGeneric<Object> res = new ResponseGeneric<>();
        	res.setEstatus(404);
        	res.setMensaje("Vuelo no encontrado.");
            return new ResponseEntity<>(res,HttpStatus.NOT_FOUND);
        }
	}
	
	@PostMapping
	public ResponseEntity<?> guardarAutorizacion(@Valid @RequestBody Vuelo vuelo, BindingResult result) {
		
		if (result.hasErrors()){
			ResponseGeneric<Object> res = new ResponseGeneric<>();
        	res.setEstatus(400);
        	res.setMensaje("Datos incorrectos.");
            ValidacionDatos validErrors = new ValidacionDatos();
            List<Map<String, Object>> listaErrores = validErrors.getListaErrores(result.getFieldErrors());
            res.setRspDatos(listaErrores);
            return new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
        }

		Vuelo vueloRegistrado = vueloService.saveVuelo(vuelo);
		ResponseGeneric<Object> res = new ResponseGeneric<>();
    	res.setEstatus(201);
    	res.setMensaje("Datos Registrados.");
    	res.setRspDatos(vueloRegistrado);
    	
        return new ResponseEntity<>(res, HttpStatus.CREATED);

	}   
	 
	 
}
