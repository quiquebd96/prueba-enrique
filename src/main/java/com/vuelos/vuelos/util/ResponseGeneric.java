package com.vuelos.vuelos.util;

import lombok.Data;

@Data
public class ResponseGeneric <T> {

	private int estatus;
    private String mensaje;
    private T rspDatos;

}
