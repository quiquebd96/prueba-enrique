package com.vuelos.vuelos.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vuelos.vuelos.entity.Vuelo;
import com.vuelos.vuelos.repository.VueloRepository;
import com.vuelos.vuelos.service.VuelosService;

@Service
public class VuelosServiceImpl implements VuelosService{

	@Autowired
	VueloRepository vueloRepository;
	
	@Override
	public Optional<Vuelo> getVueloById(Long id) {
		return vueloRepository.findById(id);
	}

	@Override
	public Vuelo saveVuelo(Vuelo vuelo) {
		return vueloRepository.save(vuelo);
	}

	@Override
	public List<Vuelo> getVuelos() {
		return vueloRepository.findAll();
	}
	
	

}
