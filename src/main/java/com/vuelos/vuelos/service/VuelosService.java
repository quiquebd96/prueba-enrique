package com.vuelos.vuelos.service;

import java.util.List;
import java.util.Optional;

import com.vuelos.vuelos.entity.Vuelo;

public interface VuelosService {

	Optional<Vuelo> getVueloById(Long id);
	Vuelo saveVuelo(Vuelo vuelo);
	List<Vuelo> getVuelos();
}
